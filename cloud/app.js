// These two lines are required to initialize Express in Cloud Code.
var express = require('express');

var app = express();

var http = require('http');
// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request body

// This is an example of hooking up a request handler with a specific request
// path and HTTP verb using the Express routing API.

// These are test functions
app.get('/test1', function(req, res) {
  var http = require('http');

  //The url we want is: 'www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
  var options = {
    host: 'www.random.org',
    path: '/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
  };

  callback = function(response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      res.send(str);
      console.log(str);
    });
  };

  http.request(options, callback).end();
});

app.post('/echo', function(req, res) {
  res.set('Content-Type', 'text/plain');
  res.send('echoing: ' + req.body.message);
});

app.post('/hello', function(req, res) {
});

// // Example reading from the request query string of an HTTP get request.
// app.get('/test', function(req, res) {
//   // GET http://example.parseapp.com/test?message=hello
//   res.send(req.query.message);
// });

// // Example reading from the request bodyf of an HTTP post request.
// app.post('/test', function(req, res) {
//   // POST http://example.parseapp.com/test (with request body "message=hello")
//   res.send(req.body.message);
// });

// Parse Function
Parse.Cloud.define("zhuo_test_func", function(request, response) {
  console.log("I am hit");
  var currentUser = Parse.User.current();
  if (currentUser) {
    response.success('user id '+currentUser.id);
  } else {
    response.success("Not Log in");
  }
});

// This is where playlist goes

app.use('/playlist', require('cloud/playlist'));
app.use('/test', require('cloud/test'));
app.use('/user', require('cloud/user'));

// Attach the Express app to Cloud Code.
app.listen();
