var roxUtils = require('cloud/roxUtils.js');
var async = require('cloud/async.js');
var _ = require('underscore');
var util = require('cloud/util.js');
var xmlreader = require('cloud/xmlreader');
var roxConstants = require('cloud/roxConstants');

exports.testJob = function(request, status) {
  // Set up to modify user data
  Parse.Cloud.useMasterKey();
  var counter = 0;
  // Query for all users
  var query = new Parse.Query(Parse.User);
  query.each(function(user) {
    status.message(user.objectId);
    // Update to plan value passed in
  }).then(function() {
    // Set the job's success status
    status.success("Migrate completed successfully.");
  }, function(error) {
    // Set the job's error status
    status.error("Uh oh, something went wrong.");
  });
};

exports.updateBillBoardTop100Tracks = function(request, status) {
  var billBoardTop100Tracks = Parse.Object.extend('billBoardTop100Tracks');
  var query = new Parse.Query('billBoardTop100Tracks');
  query.find().then(function(results) {
    // Collect one promise for each delete into an array.
    var promises = [];
    _.each(results, function(result) {
      // Start this delete immediately and add its promise to the list.
      promises.push(result.destroy());
    });
    // Return a new promise that is resolved when all of the deletes are finished.
    return Parse.Promise.when(promises);
  }).then(function() {
    Parse.Cloud.httpRequest({
      method: 'GET',
      header: {'Content-Type': 'application/json'},
      url: 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%27http%3A%2F%2Fwww.billboard.com%2Fcharts%2Fhot-100%27%20and%20xpath%3D%27%2F%2Fdiv%20%5B%40class%3D%22row-title%22%5D%20%7C%2F%2Fdiv%5B%40class%3D%22row-image%22%5D%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
    }).then(function(httpResponse) {
      // Clear the entire table
      var jsonResponse = JSON.parse(httpResponse.text);
      var createdTime = jsonResponse['query']['created'];
      var queryResults = jsonResponse.query.results.div;

      var parallesFunc = [];
      var queryResultsReshape = [];

      while(queryResults.length) queryResultsReshape.push(queryResults.splice(0,2));

      _.each(queryResultsReshape, function(result) {
        parallesFunc.push(function(callback) {
          var trackName = result[1]['h2'].replace(/\(.*\)/g, '').trim().replace('&#039;', '');

          // Assuming this will always be ArtistName? Double check
          var artistName = result[1]['h3']['a']['content'].split(' Featuring ')[0].trim();
          // Trim it background-image: url(http://www.billboard.com/images/pref_images/q24766y4tma.jpg)

          var billboardImage;
          if (result[0]['style']) {
            var styleRegex = /\((.*)\)/;
            billboardImage = styleRegex.exec(result[0]['style'])[1];
          }
          else billboardImage = result[0]['data-imagesrc'];
          roxUtils.getTrack(trackName, artistName, function(newObject) {
            var topTrackRecord = {};
            topTrackRecord['billboardImage'] = billboardImage;

            // TODO: not name/id yet
            // topTrackRecord['artistName'] = '';
            topTrackRecord['trackId'] = newObject;
            topTrackRecord['trackName'] = trackName;
            topTrackRecord['artistName'] = artistName;
            topTrackRecord['youtubeId'] = newObject.get('youtubeId');
            topTrackRecord['youtubeThumbnailDefault'] = newObject.get('youtubeThumbnailDefault');
            topTrackRecord['youtubeThumbnailMedium'] = newObject.get('youtubeThumbnailMedium');
            topTrackRecord['youtubeThumbnailLarge'] = newObject.get('youtubeThumbnailLarge');
            var billboardTrackItem = new billBoardTop100Tracks();
            billboardTrackItem.save(topTrackRecord, {
              success: function(billboardItem) {
                // console.log(billboardItem);
                callback(null, billboardItem);
              },
              error: function(billboardItem, error) {
                console.error(error.message);
                callback(null, error.message);
              }
            });
          });
        });
      });


      async.parallel(parallesFunc, function(err, results) {
        console.log(results);
        status.success('Process Success');
      });
    }, function(httpResponse) {
      // error
      status.message('Request failed with response code ' + httpResponse.status);
      status.error('failed');
    });
  });

};

exports.updateBillBoardTop200Albums = function(request, status) {
  var billBoardTop200Albums = Parse.Object.extend('billBoardTop200Albums');
  var query = new Parse.Query('billBoardTop200Albums');
  query.find().then(function(results) {
    // Collect one promise for each delete into an array.
    var promises = [];
    _.each(results, function(result) {
      // Start this delete immediately and add its promise to the list.
      promises.push(result.destroy());
    });
    // Return a new promise that is resolved when all of the deletes are finished.
    return Parse.Promise.when(promises);
  }).then(function() {
    Parse.Cloud.httpRequest({
      method: 'GET',
      header: {'Content-Type': 'application/json'},
      url: 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%27http%3A%2F%2Fwww.billboard.com%2Fcharts%2Fbillboard-200%27%20and%20xpath%3D%27%2F%2Fdiv%20%5B%40class%3D%22row-title%22%5D%20%7C%2F%2Fdiv%5B%40class%3D%22row-image%22%5D%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
    }).then(function(httpResponse) {
      // Clear the entire table
      var jsonResponse = JSON.parse(httpResponse.text);
      var createdTime = jsonResponse['query']['created'];
      var queryResults = jsonResponse.query.results.div;

      var parallesFunc = [];
      var queryResultsReshape = [];

      while(queryResults.length) queryResultsReshape.push(queryResults.splice(0,2));

      _.each(queryResultsReshape, function(result) {
        parallesFunc.push(function(callback) {
          var albumName = result[1]['h2'].replace(/\(.*\)/g, '').trim().replace('&#039;', '');

          // No Featuring has happened yet
          var artistName = result[1]['h3']['a']['content'].split(' Featuring ')[0].trim();
          // Trim it background-image: url(http://www.billboard.com/images/pref_images/q24766y4tma.jpg)

          var billboardImage;
          // Some album has no image

          // assign image
          if (result[0]['style']) {
            var styleRegex = /\((.*)\)/;
            // billboardImage = styleRegex.exec(result[0]['style'])[1];
            billboardImage = result[0]['style'];
          } else if (result[0]['data-imagesrc']) {
            billboardImage = result[0]['data-imagesrc'];
          } else {
            // No result or whatever
            // billboardImage = '';
            billboardImage = '';
          }

          roxUtils.getAlbum(albumName, artistName, function(newObject) {
            var topAlbumRecord = {};
            topAlbumRecord['billBoardImage'] = billboardImage;

            // TODO: not name/id yet
            // topAlbumRecord['artistName'] = '';
            topAlbumRecord['albumId'] = newObject;
            topAlbumRecord['albumName'] = albumName;
            topAlbumRecord['artistName'] = artistName;

            var billboardAlbumItem = new billBoardTop200Albums();
            billboardAlbumItem.save(topAlbumRecord, {
              success: function(billboardItem) {
                // console.log(billboardItem);
                callback(null, billboardItem);
              },
              error: function(billboardItem, error) {
                console.error(error.message);
                callback(null, error.message);
              }
            });
          });
        });
      });

      async.parallel(parallesFunc, function(err, results) {
        console.log(results);
        status.success('Process Success');
      });
    }, function(httpResponse) {
      // error
      status.message('Request failed with response code ' + httpResponse.status);
      status.error('failed');
    });
  });

};

exports.updateItunesTop100Tracks = function(request, status) {
  var itunesTop100 = Parse.Object.extend('itunesTop100');
  var parallelsFunc = [];
  Parse.Cloud.httpRequest({
    method: 'GET',
    header: {'Content-Type': 'application/json'},
    url: 'https://itunes.apple.com/us/rss/topsongs/limit=100/json'
  }).then(function(httpResponse) {
    var jsonResponse = JSON.parse(httpResponse.text);
    var entry = jsonResponse['feed']['entry'];
    _.each(entry, function(result) {
      parallelsFunc.push(function(callback) {
        var trackName = result['im:name']['label'];
        var itunesImage = result['im:image'][0]['label'];
        var artistName = result['im:artist']['label'];
        var youtubeQueryUrl = util.format(roxConstants.youtubeSearchUrl,
                                          trackName + ' ' + artistName).replace(/\s/g, '%20');
        Parse.Cloud.httpRequest({
          url: youtubeQueryUrl,
          method: 'GET',
          header: {'Content-Type': 'application/json'}
        }).then(function(httpResponse) {
          var youtubeItem = JSON.parse(httpResponse.text)['items'][0];
          var newItunesItem = {};
          newItunesItem['artistName'] = artistName;
          newItunesItem['itunesImage'] = itunesImage;
          newItunesItem['trackName'] = trackName;
          try {
            var snippet = youtubeItem['snippet'];
            newItunesItem['youtubeThumbnailLarge'] = snippet['thumbnails']['high']['url'];
            newItunesItem['youtubeThumbnailMedium'] = snippet['thumbnails']['medium']['url'];
            newItunesItem['youtubeThumbnailDefault'] = snippet['thumbnails']['default']['url'];
            newItunesItem['youtubeId'] = youtubeItem['id']['videoId'];
          } catch (e) {
            console.log(httpResponse.text);
          }
          var itunesTop100Item = new itunesTop100();
          itunesTop100Item.save(newItunesItem,  {
            success: function(ituneItem) {
              // console.log(ituneItem);
              callback(null, newItunesItem);
            },
            error: function(ituneItem, error) {
              console.error(error.message);
              callback(null, error.message);
            }
          });
        });
      });
    });
    async.parallel(parallelsFunc, function(err, results) {
      status.success('Process Success');
    });
  });
};

exports.updateOfficialChartTop100Tracks = function(request, status) {
  //  var regex = /<div class=\"track\">\s+<div class=\"cover\" style=\".*\">\s+<img src=\"(.*)\" alt=\".*\" width=\"60\" height=\"60\"\/>\s+<\/div>\s+<div class=\"title-artist\">\s+<div class=\"title\">\s+<a href=\".*\">(.*)<\/a>\s+<\/div>\s+<div class=\"artist\">\s+<a href=\".*\">(.*)<\/a>\s+<\/div>/gm;
  var query = new Parse.Query('officialChartTop100Tracks');
  query.find().then(function(results) {
    // Collect one promise for each delete into an array.
    var promises = [];
    _.each(results, function(result) {
      // Start this delete immediately and add its promise to the list.
      promises.push(result.destroy());
    });
    // Return a new promise that is resolved when all of the deletes are finished.
    return Parse.Promise.when(promises);
  }).then(function() {
    var regex = /<div class=\"track\">\s+<div class=\"cover\" style=\".*\">\s+<img src=\"(.*)\" alt=\".*\" width=\"60\" height=\"60\"\/>\s+<\/div>\s+<div class=\"title-artist\">\s+<div class=\"title\">\s+<a href=\".*\">(.*)<\/a>\s+<\/div>\s+<div class=\"artist\">\s+<a href=\".*\">(.*)<\/a>\s+<\/div>/g;
    var officialChartTop100Tracks = Parse.Object.extend('officialChartTop100Tracks');

    Parse.Cloud.httpRequest({
      method: 'GET',
      'Content-Type': 'application/text;charset=utf-8',
      url: 'http://www.officialcharts.com/charts/singles-chart/'
    }).then(function(httpResponse) {
      var parallelsFunc = [];
      var body = httpResponse.text;
      var match;
      var matchResults = [];
      while (match = regex.exec(body)) {
        matchResults.push(match);
      }
      _.each(matchResults, function(match){
        var tempImg = match[1];
        var tempTrackName = match[2];
        var tempArtistName = match[3];
        parallelsFunc.push(function(callback) {
          var officialChartImage = tempImg;
          var trackName = tempTrackName;
          var artistName = tempArtistName;
          var youtubeQueryUrl = util.format(roxConstants.youtubeSearchUrl,
                                            trackName + ' ' + artistName).replace(/\s/g, '%20');
          Parse.Cloud.httpRequest({
            url: youtubeQueryUrl,
            method: 'GET',
            header: {'Content-Type': 'application/json'}
          }).then(function(httpResponse) {
            var youtubeItem = JSON.parse(httpResponse.text)['items'][0];
            var newofficialChartItem = {};
            newofficialChartItem['artistName'] = artistName;
            newofficialChartItem['officialChartImage'] = officialChartImage ;
            newofficialChartItem['trackName'] = trackName;
            var snippet = youtubeItem['snippet'];
            newofficialChartItem['youtubeThumbnailLarge'] = snippet['thumbnails']['high']['url'];
            newofficialChartItem['youtubeThumbnailMedium'] = snippet['thumbnails']['medium']['url'];
            newofficialChartItem['youtubeThumbnailDefault'] = snippet['thumbnails']['default']['url'];
            newofficialChartItem['youtubeId'] = youtubeItem['id']['videoId'];

            var officialChartTop100TracksItem = new officialChartTop100Tracks();
            officialChartTop100TracksItem.save(newofficialChartItem,  {
              success: function(officialChartItem) {
                // console.log(ituneItem);
                callback(null, officialChartItem);
              },
              error: function(ituneItem, error) {
                console.error(error.message);
                callback(null, error.message);
              }
            });
          });
        });
      });

      console.log(parallelsFunc.length);
      async.parallel(parallelsFunc, function(err, results) {
        status.success('Process Success');
      });
    });
  });

};
