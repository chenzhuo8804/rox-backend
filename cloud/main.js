
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:

// var require2 = require("cloud/parse_modules/require/index");
require("cloud/app");
var jobs = require('cloud/jobs.js');

// Parse.initialize("ZqnlHHot3sMQIrQUmDqnQgXNGDz3J2RQcYWiZhp8", "JlE3yXlVkYotda493IysFZ6PsEHcKsKBgoQ1Yb9d");
/*Parse.Cloud.define("hello", function(request, response) {
 response.success("Hello world!");
 });*/

/*Parse.Cloud.define("averageStars", function(request, response) {
 var query = new Parse.Query("Review");
 query.equalTo("movie", request.params.movie);
 query.find({
 success: function(results) {
 var sum = 0;
 for (var i = 0; i < results.length; ++i) {
 sum += results[i].get("stars");
 }
 response.success(sum / results.length);
 },
 error: function() {
 response.error("movie lookup failed");
 }
 });
 });*/

Parse.Cloud.job("testJob", jobs.testJob);
Parse.Cloud.job("updateBillBoardTop100Tracks", jobs.updateBillBoardTop100Tracks);
Parse.Cloud.job("updateBillBoardTop200Albums", jobs.updateBillBoardTop200Albums);
Parse.Cloud.job("updateItunesTop100Tracks", jobs.updateItunesTop100Tracks);
Parse.Cloud.job("updateOfficialChartTop100Tracks", jobs.updateOfficialChartTop100Tracks);
