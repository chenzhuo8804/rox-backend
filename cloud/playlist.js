// Example request to echoNest http://developer.echoNest.com/api/v4/playlist/static?api_key=AAYP6KXE4XIPLWZKM&seed_catalog=CAZVZPL14B5C28B2DC&type=catalog-radio&results=10

var async = require('cloud/async');
var _ = require('underscore');

var echoNestUrlPlaylistId = 'http://developer.echoNest.com/api/v4/playlist/static?api_key=AAYP6KXE4XIPLWZKM&seed_catalog={0}&type=catalog-radio&results=10';


var test_url = 'http://developer.echoNest.com/api/v4/playlist/static?api_key=AAYP6KXE4XIPLWZKM&seed_catalog=CAZVZPL14B5C28B2DC&type=catalog-radio&results=10';


var echoNestPlaylistUrl = 'http://developer.echoNest.com/api/v4/playlist/static';
var echoNestApiKey = "AAYP6KXE4XIPLWZKM";
var echoNest_seed_catalog = 'CAZVZPL14B5C28B2DC';

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var spotify_album_url = 'https://api.spotify.com/v1/albums/';

function requestParamsToStr(params) {
  var result_arr = [];
  for (var key in params) {
    // if (params[key].constructor == Array) {
    if (_.isArray(params[key])) {
      for (var item in params[key]) {
        result_arr.push(key.concat('=', params[key][item].toString()));
      }
    } else {
      result_arr.push(key.concat('=', params[key].toString()));
    }
  }
  return result_arr.join('&');
}

var param = {
  api_key : echoNestApiKey,
  seed_catalog : echoNest_seed_catalog,
  type : 'catalog-radio',
  results : 10,
  bucket : ['id:spotify', 'tracks']
};

function updatePlaylist(user) {
  var adventurousness = user.get('adventurousness') ? user.get('adventurousness') : '1';
  var variety = user.get('variety') ? user.get('variety') : '1';
  var echoNestTasteId = user.get('echoNestTasteId');
  var params = {};
  if (echoNestTasteId) {
    params = {api_key: echoNestApiKey, seed_catalog: echoNestTasteId,
              type: 'catalog-radio', results: 100,
              bucket: ['song_type', 'audio_summary']};
  } else { // No echoNest id provided
  }
  var urlString = [echoNestPlaylistUrl, requestParamsToStr(params)].join('?');
  // Parse.Cloud.httpRequest({
  //   url: urlString,
  //   method: 'GET',
  //   header: {'Content-Type': 'application/json'}
  // }).then(function(httpResponse) {
  //   console.log(httpResponse);
  // }, function(httpResponse) {
  //   console.log('error: ' + httpResponse);
  // });

  Parse.Cloud.httpRequest({
    url: urlString, method: 'GET', header: {'Content-Type': 'application/json'},
    success: function(httpResponse) {
      console.log(httpResponse);
      return;
    },
    error: function(httpResponse) {
      console.log('error' + httpResponse);
      return;
    }
  });
}

module.exports = function() {
  var express = require('express');
  var app = express();
  // console.log(requestParamsToStr(param));
  var url = [echoNestPlaylistUrl, requestParamsToStr(param)].join('?');
  app.get('/new', function(request, response){
    var objectId = request.query.id;
    var query = new Parse.Query(Parse.User);
    query.equalTo('objectId', objectId);
    query.find().then(function(user) {
      // Test if echoNestTasteId is empty array
      // if (typeof user.echoNestTasteId !== 'undefined' && user.echoNestTasteId.length > 0) {
      if (typeof user.echoNestTasteId !== 'undefined' && user.echoNestTasteId.length > 0) {
        // TODO: Find a better way to construct String
        var urlStr = echoNestUrlPlaylistId.format(user.echoNestTasteId);
        Parse.Cloud.httpRequest({
          url: urlStr,
          method: 'GET',
          header:{
            'Content-Type': 'application/json'
          }
        }).then(function(httpResponse){
          response.send(httpResponse);
        });
      } else { // without echoNestid
        var params = {
          api_key : echoNestApiKey,
          type : 'song-radio',
          results : 10
        };
        if (user[0].get('likedArtists')) {
          params['artist_id'] = user[0].get('likedArtists');
        }
        if (user[0].get('likedTracks')) {
          params['song_id'] = user[0].get('likedTracks');
        }
        // TODO: find a way to get array instead

        var urlString = [echoNestPlaylistUrl, requestParamsToStr(params)].join('?');
        console.log(urlString);
        Parse.Cloud.httpRequest({
          url: urlString,
          method: 'GET',
          header: {
            'Content-Type': 'application/json'
          }
        }).then(function(httpResponse) {
          response.json(JSON.parse(httpResponse.text));
        });
      }
    });
  });

  app.get('/', function(request, response) {
    var sessionToken = request.query.sessionToken;
    Parse.User.become(sessionToken).then(function(user) {
      updatePlaylist(user);
      response.send('success');
    });
    // if (true) {
    //   Parse.Cloud.httpRequest({
    //     url: url,
    //     method: 'GET',
    //     header:{
    //       'Content-Type': 'application/json'
    //     },
    //     success: function(httpResponse) {
    //       response_arr = [];
    //       image_task_arr = [];
    //       json_result = JSON.parse(httpResponse.text);
    //       // This way we fill the album with spotify information (Image primarily)
    //       json_result.response.songs.forEach(function(song_item){
    //         var spotify_id = '';
    //         // Get spotifyId of current track

    //         // We cant use forEach here since we need to break the loop at some point
    //         // song_item.tracks.forEach(function(track_item) {
    //         //   if (track_item.hasOwnProperty('album_name') &&
    //         //       track_item.catalog == 'spotify') {
    //         //     spotify_id = track_item.foreign_id.sub_string(14);
    //         //     break;
    //         //   }
    //         // });
    //         for (var track_id in song_item.tracks) {
    //           var track_item = song_item.tracks[track_id];
    //           if (/* track_item.hasOwnProperty('album_name') && */
    //             track_item.catalog == 'spotify') {
    //             spotify_id = track_item.foreign_release_id.substring(14);
    //             break;
    //           }
    //         }

    //         var spotify_request_url = spotify_album_url + spotify_id;
    //         // console.log(spotify_request_url

    //         // console.log(spotify_request_url);

    //         // We have 10 more requests to send here!
    //         image_task_arr.push(function(callback) {
    //           Parse.Cloud.httpRequest({
    //             url: spotify_request_url,
    //             method: 'GET',
    //             header:{
    //               'Content-Type': 'application/json'
    //             },
    //             success: function(httpResponse) {
    //               spotify_json_res = JSON.parse(httpResponse.text);
    //               song_item.image_url = spotify_json_res.images[1].url;

    //               response_arr.push(song_item);
    //               callback(null);
    //             },
    //             error: function(httpResponse) {
    //               // console.log(httpResponse);
    //               callback(null);
    //             }
    //           });
    //         });
    //       });
    //       async.parallel(image_task_arr, function(err){
    //         response.json({
    //           response: response_arr
    //         });
    //       });
    //     },
    //     error: function(httpResponse) {
    //       // console.log(httpResponse);
    //     }
    //   });
    // }
  });
  //    response.json({result: 'Just a result'}); // 2nd response
  return app;
}();
