function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("spotifyTrackWithNameAndArtistUrl", 'https://api.spotify.com/v1/search?query=%s+artist%3A%s+&offset=0&limit=5&type=track');

define('spotifyAlbumWithNameAndArtistUrl', 'https://api.spotify.com/v1/search?query=album:%s+artist:$s&limit=5&type=album');

define('youtubeSearchUrl', 'https://www.googleapis.com/youtube/v3/search?order=relevance&part=snippet&q=%s&type=video&key=AIzaSyC0LiDxD14XLHgMnRlmjLyiStHEM1Orb-o');

