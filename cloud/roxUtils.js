var util = require('cloud/util.js');
// Section for constants
var roxConstant = require('cloud/roxConstants.js');
var async = require('cloud/async.js');

var trackHashString = function(trackName, artistName) {
  var hashStr = util.format('%s;%s', trackName, artistName);
  return hashStr.toLowerCase();
};

var albumHashString = function(albumName, artistName) {
  var hashStr = util.format('%s;%s', albumName, artistName);
};

// try to get this track from track table, if no track is found, create track in track table and return
var getTrack = function(trackName, artistName, callback) {
  // generate Hash Column
  var hashValue = trackHashString(trackName, artistName);
  var trackRecord = Parse.Object.extend('Track');
  var trackQuery = new Parse.Query(trackRecord);
  trackQuery.equalTo('billBoardHash', hashValue);
  trackQuery.first({
    success: function(object) {
      if (object) {
        callback(object);
      } else {
        var spotifyQuery = function(spotifyCallback) {
          var url = util.format(roxConstant.spotifyTrackWithNameAndArtistUrl,
                                trackName, artistName).replace(/\s/g, '%20');
          Parse.Cloud.httpRequest({
            url: url,
            method: 'GET'
            // header: {'Content-Type': 'application/json'}
          }).then(function(httpResponse) {
            // Now we only care about the first record
            var result = {};
            // record found
            var spotifyItems = JSON.parse(httpResponse.text)['tracks']['items'];
            if (spotifyItems.length != 0) {
              var spotifyItem = JSON.parse(httpResponse.text)['tracks']['items'][0];
              result['trackName'] = spotifyItem['name'].replace(/\(.*\)/g, '').split(' - ')[0].trim();
              result['albumName'] = spotifyItem['album']['name'];

              var artists = spotifyItem['artists'][0]['name'].split(' & ')[0].trim();

              result['spotifyId'] = spotifyItem['id'];

              // console.log('hash' + trackHashString(result['trackName'], artists));
              result['hashColumn'] = trackHashString(result['trackName'],
                                                     artists);

              // console.log('billBoardHash: ' + trackHashString(trackName, artistName));
              result['billBoardHash'] = trackHashString(trackName, artistName);
            } else {
              // No record regarding spotify is found
              result['trackName'] = trackName;
              result['billBoardHash'] = trackHashString(trackName, artistName);
            }

            spotifyCallback(null, result);
          }, function(httpResponse) {
            var result = {};
            result['trackName'] = trackName;
            result['billBoardHash'] = trackHashString(trackName, artistName);
            spotifyCallback(null, result);
          });
        };

        var youtubeQuery = function(youtubeCallback) {
          var url = util.format(roxConstant.youtubeSearchUrl,
                                trackName + ' ' + artistName).replace(/\s/g, '%20');
          Parse.Cloud.httpRequest({
            url: url,
            method: 'GET',
            header: {'Content-Type': 'application/json'}
          }).then(function(httpResponse) {
            // Now we only care about the first record
            var youtubeItem = JSON.parse(httpResponse.text)['items'][0];
            var result = {};
            result['youtubeId'] = youtubeItem['id']['videoId'];
            result['youtubeThumbnailDefault'] = youtubeItem['snippet']['thumbnails']['default']['url'];
            result['youtubeThumbnailMedium'] = youtubeItem['snippet']['thumbnails']['medium']['url'];
            result['youtubeThumbnailLarge'] = youtubeItem['snippet']['thumbnails']['high']['url'];
            youtubeCallback(null, result);
          }, function(httpResponse) {
            // error
            youtubeCallback(null, {});
            // console.log('Youtube Request failed ' + url);
          });
        };
        async.parallel({
          youtube: youtubeQuery,
          spotify: spotifyQuery
        }, function(err, results) {
          var newTrackRecord = {};
          for (var attrname in results['youtube']) {
            newTrackRecord[attrname] = results['youtube'][attrname];
          }
          for (var attrname in results['spotify']) {
            newTrackRecord[attrname] = results['spotify'][attrname];
          }
          var newTrack = new trackRecord();
          newTrack.save(newTrackRecord).then(function(obj) {
            callback(obj);
            return true;
          }, function(error) {
            console.log('Error saving new track data');
            return false;
          });
        });
      }
    },
    error: function(error) {
      // TODO: Printing error message and do something
    }
  });
};

var getAlbum = function(albumName, artistName, callback) {
  // generate Hash Column
  var hashValue = trackHashString(albumName, artistName);
  var albumRecord = Parse.Object.extend('Album');
  var albumQuery = new Parse.Query(albumRecord);

  // This is using billboardHash
  albumQuery.equalTo('billBoardHash', hashValue);
  albumQuery.first({
    success: function(object) {
      if (object) {
        callback(object);
      } else {
        var spotifyQuery = function(spotifyCallback) {
          var url = util.format(roxConstant.spotifyAlbumWithNameAndArtistUrl,
                                albumName, artistName).replace(/\s/g, '%20');
          Parse.Cloud.httpRequest({
            url: url,
            method: 'GET'
            // header: {'Content-Type': 'application/json'}
          }).then(function(httpResponse) {
            // Now we only care about the first record
            var result = {};
            // record found
            var spotifyItems = JSON.parse(httpResponse.text)['albums']['items'];
            if (spotifyItems.length != 0) {
              var spotifyItem = JSON.parse(httpResponse.text)['tracks']['items'][0];
              result['albumName'] = spotifyItem['name'].replace(/\(.*\)/g, '').split(' - ')[0].trim();

              // we dont take care of artist, artistName is from 
              // result['artistName'] = artistName;

              // var artists = spotifyItem['artists'][0]['name'].split(' & ')[0].trim();

              result['spotifyId'] = spotifyItem['id'];

              // console.log('hash' + trackHashString(result['trackName'], artists));

              // dont worry about hashColum now
              // result['hashColumn'] = trackHashString(result['albumName'], '');

              // console.log('billBoardHash: ' + trackHashString(trackName, artistName));
              result['billBoardHash'] = trackHashString(albumName, artistName);
            } else {
              // No record regarding spotify is found
              result['albumName'] = albumName;
              result['billBoardHash'] = trackHashString(albumName, artistName);
            }

            spotifyCallback(null, result);
          }, function(httpResponse) {
            var result = {};
            result['albumName'] = albumName;
            result['billBoardHash'] = trackHashString(albumName, artistName);
            spotifyCallback(null, result);
          });
        };

        async.parallel({
          spotify: spotifyQuery
        }, function(err, results) {
          var newAlbumRecord = {};
          for (var attrname in results['spotify']) {
            newAlbumRecord[attrname] = results['spotify'][attrname];
          }
          var newAlbum = new albumRecord();
          newAlbum.save(newAlbumRecord).then(function(obj) {
            callback(obj);
            return true;
          }, function(error) {
            console.log('Error saving new track data');
            return false;
          });
        });
      }
    },
    error: function(error) {
      // TODO: Printing error message and do something
    }
  });
};

module.exports = {
  getTrack: getTrack,
  getAlbum: getAlbum
};


