// This file is the test module

require('http');
module.exports = function(){
  var express = require('express');
  var app = express();
  app.get('/test1', function(req, res){
    var query = new Parse.Query(Parse.User);
    query.find({
      success: function(results) {
        var currentUser = Parse.User.current();
        console.log(currentUser);
        for (var i = 0; i<results.length; i++) {
          console.log(results[i]);
        }

        res.send(results.length);
      }
    });
  });

  app.get('/test2', function(req, res){
    var request_url = 'http://echo.jsontest.com/key/value/one/two';
    Parse.Cloud.httpRequest({
      url: request_url,
      method: 'GET',
      header:{
        'Content-Type': 'application/json'
      }
    }).then(function(result){
      res.send(result); 
    });
  });

  app.get('/test3', function(req, res){
    var id = 'NUNQaKfT96';
    var query = new Parse.Query(Parse.User);
    query.equalTo('objectId', id);
    query.find().then(function(user) {
      res.send(user);
    });
  });

  app.get('/alluser', function(req, res) {
    var query = new Parse.Query(Parse.User);
    query.find().then(function(users) {
      for(var i = 0; i < users.length; i++) {
        console.log(users);
      }
    });
  });
  return app;
}();
