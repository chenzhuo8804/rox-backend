// TODO: Move these urls to externalConstant
var echonestUpdateTasteUrl = 'http://developer.echonest.com/api/v4/tasteprofile/update';
var echonestCreateTasteUrl = 'http://developer.echonest.com/api/v4/tasteprofile/create';
var echonestAPIKey = 'AAYP6KXE4XIPLWZKM';

module.exports = function() {
  var express = require('express');
  var app = express();


  // Get likedArtists
  app.get('/taste/artist', function(request, response) {
    var sessionToken = request.query.sessionToken;
    Parse.User.become(sessionToken).then(function (user) {
      var likedArtistsList = JSON.parse(user.get('likedArtistJSON'));
      response.json({username: user.get('username'),
                     artists: likedArtistsList});
    });
  });

  // Get likedTracks
  app.get('/taste/tracks', function(request, response) {
    var sessionToken = request.query.sessionToken;
    Parse.User.become(sessionToken).then(function (user) {
      var likedTracksList = JSON.parse(user.get('likedTracksJSON'));
      response.json({username: user.get('username'),
                     tracks: likedTracksList});
    });
  });

  // Get likedArtists + likedTracks
  app.get('/taste', function(request, response) {
    var sessionToken = request.query.sessionToken;
    Parse.User.become(sessionToken).then(function (user) {
      var likedArtistsList = JSON.parse(user.get('likedArtistJSON'));
      var likedTrackList = JSON.parse(user.get('likedTrackJSON'));
      response.json({username: user.get('username'),
                     tracks: likedTracksList,
                     artists: likedArtistsList});
    });
  });

  // Update your taste for a track
  app.post('/taste/track', function(request, response){
    postBody = request.body;
    var itemId = postBody.itemId;
    var stars = postBody.stars;

    var sessionToken = postBody.sessionToken;
    Parse.User.become(sessionToken).then(function (user) {
      // Case w/o tasteId
      var echoNestTasteId = user.get('echoNestTasteId');
      if (stars > 3) {
        var likedArtists = JSON.parse(user.get('likedArtistsJSON'));
        var likedTracks = JSON.parse(user.get('likedTracksJSON'));
        if (likedTracks.indexOf(itemId) == -1) {
          likedTracks.push({echonestId: itemId, stars: stars});
          user.set("likedTracksJSON", JSON.stringify(likedTracks));
        } else {
          // TODO: more elegent response
          // TODO: return stuff here
        }

        if (echoNestTasteId) {
          var param = [{action: 'update', tasteType: 'track'}];
          Parse.Cloud.httpRequest({
            method: 'POST', url: echonestUpdateTasteUrl, body: param,
            success: function(httpResponse) {
              user.save(null, {
                success: function(user) {
                  // TODO: nicer response
                  successRes = {status: 'success', echonestTasteId: tasteId};
                  response.json(successRes);
                },
                error: function(user, error) {
                  // TODO: nicer response
                  console.error('error: ' + error);
                  errorRes = {
                    status: 'error',
                    error: error
                  };
                  response.json(errorRes);
                }
              });
              // TODO: Make a nicer response

            },
            error: function(httpResponse) {
              console.error('error: ' + httpResponse.text);
              response.send('error: ' + httpResponse);
            }
          });
        } else {
          var count = likedTracks.length + likedArtists.length;
          console.log('log' + count);
          if (count > 5) {
            // TODO: get a echonest id for this record
            var tasteName = user.get('username') + 'taste';
            var params = {api_key: echonestAPIKey, format: 'json',
                          type: 'general', name: tasteName};
            console.log('tastename: '+ tasteName);
            Parse.Cloud.httpRequest({
              method: 'POST', url: echonestCreateTasteUrl, body: params
            }).then(function(httpResponse){
              var tasteId = httpResponse.data.response.status.id;
              console.log('tasteid: ' + tasteId);
              user.set('echoNestTasteId', tasteId);
              // Update taste profile with existing tastes
              actions = [];
              likedArtists.forEach(function(artist_id) {
                var entry = {action: 'update', item: {artist_id: artist_id}};
                actions.push(entry);
              });
              likedTracks.forEach(function(track_id){
                var entry = {action: 'update', item: {song_id: track_id}};
                actions.push(entry);
              });
              var params = {
                api_key: echonestAPIKey, data_type: 'json', format: 'json',
                id: tasteId, data: JSON.stringify(actions)
              };
              Parse.Cloud.httpRequest({
                method: 'POST', url: echonestUpdateTasteUrl, body: params,
                success: function(httpResponse) {
                  user.save(null, {
                    success: function(user) {
                      // TODO: nicer response
                      successRes = {status: 'success', echonestTasteId: tasteId};
                      response.json(successRes);
                    },
                    error: function(user, error) {
                      // TODO: nicer response
                      console.error('error: ' + error);
                      errorRes = {status: 'error', error: error};
                      response.json(errorRes);
                    }
                  });
                  // TODO: Make a nicer response

                },
                error: function(httpResponse) {
                  console.error('error: ' + httpResponse.text);
                  response.send('error: ' + httpResponse);
                }
              });
            }, function(error) {
              console.error('error' + error);
              response.send('error: ' + error);
            });
          }
        }
      } else if (stars < 3) {
        
      }
    });
  });

  
  app.post('/taste', function(request, response){
    postBody = request.body;
    var itemId = postBody.itemId;
    var tasteType = postBody.tasteType;

    var sessionToken = postBody.sessionToken;
    Parse.User.become(sessionToken).then(function (user) {
      // Case w/o tasteId
      var likedArtists = JSON.parse(user.get('likedArtistsJSON'));
      var likedTracks = JSON.parse(user.get('likedTracksJSON'));
      var echoNestTasteId = user.get('echoNestTasteId');
      if (tasteType == 'track') {
        if (likedTracks.indexOf(itemId) == -1) {
          likedTracks.push(itemId);
          user.set("likedTracksJSON", JSON.stringify(likedTracks));
        } else {
          // TODO: more elegent response
          // TODO: return stuff here
        }
      } else if (tasteType == 'artist') {
        if(likedArtists.indexOf(itemId) == -1) {
          likedArtists.push(itemId);
          user.set('likedArtistsJSON', JSON.stringify(likedArtists));
        }
      }

      if (echoNestTasteId) {
        var param = [{action: 'update', tasteType: tasteType}];
        Parse.Cloud.httpRequest({
          method: 'POST',
          url: echonestUpdateTasteUrl,
          body: param,
          success: function(httpResponse) {
            user.save(null, {
              success: function(user) {
                // TODO: nicer response
                successRes = {
                  status: 'success',
                  echoNestTasteId: tasteId
                };
                response.json(successRes);
              },
              error: function(user, error) {
                // TODO: nicer response
                console.error('error: ' + error);
                errorRes = {
                  status: 'error',
                  error: error
                };
                response.json(errorRes);
              }
            });
            // TODO: Make a nicer response

          },
          error: function(httpResponse) {
            console.error('error: ' + httpResponse.text);
            response.send('error: ' + httpResponse);
          }
        });
      } else {
        var count = likedTracks.length + likedArtists.length;
        console.log('log' + count);
        if (count > 5) {
          // TODO: get a echonest id for this record
          var tasteName = user.get('username') + 'taste';
          var params = {
            api_key: echonestAPIKey,
            format: 'json',
            type: 'general',
            name: tasteName
          };
          console.log('tastename: '+ tasteName);
          Parse.Cloud.httpRequest({
            method: 'POST',
            url: echonestCreateTasteUrl,
            body: params
          }).then(function(httpResponse){
            var tasteId = httpResponse.data.response.status.id;
            console.log('tasteid: ' + tasteId);
            user.set('echoNestTasteId', tasteId);
            // Update taste profile with existing tastes
            actions = [];
            likedArtists.forEach(function(artist_id) {
              var entry = {action: 'update', item: {artist_id: artist_id}};
              actions.push(entry);
            });
            likedTracks.forEach(function(track_id){
              var entry = {action: 'update', item: {song_id: track_id}};
              actions.push(entry);
            });
            var params = {
              api_key: echonestAPIKey,
              data_type: 'json',
              format: 'json',
              id: tasteId,
              data: JSON.stringify(actions)
            };
            Parse.Cloud.httpRequest({
              method: 'POST',
              url: echonestUpdateTasteUrl,
              body: params,
              success: function(httpResponse) {
                user.save(null, {
                  success: function(user) {
                    // TODO: nicer response
                    successRes = {
                      status: 'success',
                      echoNestTasteId: tasteId
                    };
                    response.json(successRes);
                  },
                  error: function(user, error) {
                    // TODO: nicer response
                    console.error('error: ' + error);
                    errorRes = {
                      status: 'error',
                      error: error
                    };
                    response.json(errorRes);
                  }
                });
                // TODO: Make a nicer response

              },
              error: function(httpResponse) {
                console.error('error: ' + httpResponse.text);
                response.send('error: ' + httpResponse);
              }
            });
          }, function(error) {
            console.error('error' + error);
            response.send('error: ' + httpResponse);
          });
        }
      }
    });
  });

  app.post('/history', function(request, response){
    // This request allows to post one history item
    // TODO: Write something that allows batch
    var postBody = request.body;
    var playedItem = postBody.item;
    var time = postBody.time;
    var sessionToken = postBody.sessionToken;
    // TODO: No validation made so far
    console.log(sessionToken);
    Parse.User.become(sessionToken).then(function (user) {
      var historyString = user.get('history');
      // If empty string
      if (!historyString) {
        console.log('reset' + historyString);
        historyString = '[]';
      }
      var historyList = JSON.parse(historyString);
      var item = {echonestId: playedItem, playedTime: time};
      historyList.push(item);
      console.log('historyList: ' + historyList);
      if (historyList.length > 100) {
        historyList.shift();
      }
      user.set('history', JSON.stringify(historyList));
      user.save(null, {
        success: function(user) {
          // TODO: better return 
          response.send('user updated successfully');
        },
        error: function(user, error) {
          console.error('Erro occurs when saving user: ' + error);
          response.send('error');
        }
      });
    });
  });

  app.get('/history', function(request, response) {
    var sessionToken = request.query.sessionToken;
    console.log('sessionToken: ' + sessionToken);
    Parse.User.become(sessionToken).then(function (user) {
      var historyArray = JSON.parse(user.get('history'));
      console.log('array: ' + historyArray);
      var userId = user.get('username');
      console.log('username: ' + userId);
      response.json({username: userId, history: historyArray});
    });
  });
  return app;
}();


